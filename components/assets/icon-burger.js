import React, { useContext } from 'react';
import styles from '../../styles/header.module.scss'
import ThemeContext from '../../constants/theme-context';

export default function IconBurger (props) {
  const { theme } = useContext(ThemeContext)
  const fillColor = theme === 'dark' ? '#FFFFFF' : '#000000'
  const iconClassNames = [styles.icon_burger, styles[`icon_burger_${theme}`]]
  const handleClicked = () => {
    if (props.onClick) props.onClick()
  }
  return (
    <a href="#" className={iconClassNames.join(" ")} onClick={handleClicked}>
      <svg width="20px" height="15px" viewBox="0 0 24 15">
        <g id="Detail-Light" transform="translate(-56.000000, -35.000000)" fill={fillColor}>
          <path d="M79.25,37.52 L79.25,35 L56.75,35 L56.75,37.52 L79.25,37.52 Z M79.25,43.73 L79.25,41.27 L56.75,41.27 L56.75,43.73 L79.25,43.73 Z M79.25,50 L79.25,47.48 L56.75,47.48 L56.75,50 L79.25,50 Z" id=""></path>
        </g>
      </svg>
    </a>
  )
}