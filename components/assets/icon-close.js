import React, { useContext } from 'react';
import styles from '../../styles/header.module.scss'
import ThemeContext from '../../constants/theme-context';

export default function IconClose (props) {
  const { theme } = useContext(ThemeContext)
  const fillColor = theme === 'dark' ? '#FFFFFF' : '#000000'
  const iconClassNames = [styles.close_button]
  const handleClicked = () => {
    if (props.onClick) props.onClick()
  }
  return (
    <a href="#" className={iconClassNames.join(" ")} onClick={handleClicked}>
      <svg width="20px" height="40px" viewBox="0 0 30 15">
        <g id="Detail-Light" fill={fillColor}>
          <path d="M18.695,16l6.752-6.752a1.886,1.886,0,0,0,0-2.668l-.027-.027a1.886,1.886,0,0,0-2.668,0L16,13.305,9.248,6.553a1.886,1.886,0,0,0-2.668,0l-.027.027a1.886,1.886,0,0,0,0,2.668L13.305,16,6.553,22.752a1.886,1.886,0,0,0,0,2.668l.027.027a1.886,1.886,0,0,0,2.668,0L16,18.695l6.752,6.752a1.886,1.886,0,0,0,2.668,0l.027-.027a1.886,1.886,0,0,0,0-2.668Z" id=""></path>
        </g>
      </svg>
    </a>
  )
}