import React from 'react';

export default function IconArrowRight (props) {
  return (
    <svg width="10px" height="16px" viewBox="0 0 10 16">
      <polygon points="2.48 15.7 9.98 8.2 2.48 0.7 0.71 2.47 6.47 8.2 0.71 13.96"></polygon>
    </svg>
  )
}

