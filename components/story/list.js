import Card from '../content/card'
import ContentList from '../content/list'
import Pagination from '../content/pagination'

export default function List({ data, page = 1, link, showAuthor = true }) {
  return (
    <>
      <ContentList>
        {data.items.map((item) => {
          return <Card key={`item-${item.id}`} item={item} showAuthor={showAuthor} />
        })}
      </ContentList>

      <Pagination count={data.meta.total_count} link={link} page={page} />
    </>
  )
}
