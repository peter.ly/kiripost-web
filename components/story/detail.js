import React, { useContext, useState } from 'react';
import styles from '../../styles/story.module.scss'
import PageNotFound from '../layout/page-not-found'
import { longDateFormat } from '../../library/date'
import Related from '../content/related';
import ShareTo from '../content/share';
import ThemeContext from '../../constants/theme-context';

export default function Detail({ data, latestStories, relatedTopicStories, featuredStories }) {
  const { theme } = useContext(ThemeContext)
  if (!data) return <PageNotFound />
  const authorName = (author) => {
    return author.first_name && author.last_name ? `${author.first_name} ${author.last_name}` : author.username
  }
  const isTopicOrCollection = (data.topics.length > 0 || data.collections.length > 0)
  return (
    <div className={styles.container}>
      <div className={styles.block_title}>
        {data.is_sponsored && <span className={styles.sponsored_tag}>Sponsored</span>}
        <div className={styles.block_atitle}>
          <span className={styles.atitle}>{data.alt_title}</span>
        </div>
        <h1 className={styles.title}><span className={[styles.highlight, styles[`${theme}`]].join(" ")}>{data.title}</span></h1>
      </div>
      <div className={styles.meta}>
        {data.authors && (
          <div className={styles.authors}>
            <span>By:</span> <strong>{data.authors.map((item, i) => <span key={i}>{authorName(item)}</span>).reduce((prev, curr) => [prev, ', ', curr])}</strong>
          </div>
        )}
        <div className="pubdate"><span>&nbsp;</span>
          <span> on </span> <span className="dt">{longDateFormat(data.published_at)}</span>
        </div>
        <span className={styles.readtime}> ~ {data.readtime} min read</span>
      </div>
      <div className={[styles.intro, styles[`${theme}`]].join(" ")}>{data.intro}</div>
      {data.cover && (
        <div className={styles.block_cover}>
          <figure className={[styles.cover, styles[`${theme}`]].join(" ")}>
            <div className={[styles.figure_content, styles[`${theme}`]].join(" ")}>
              <img src={data.cover.url} alt={data.cover.alt} />
            </div>
            <figcaption className="caption">{data.cover.alt}</figcaption>
          </figure>
        </div>
      )}
      <div className={styles.body_wrapper}>
        <aside className={styles.gutter}>
          <ShareTo page={data} orientation="vertical" services={['facebook', 'twitter', 'linkedin', 'link']} />
        </aside>
        <main className={[styles.main, styles[`${theme}`]].join(" ")}>
          <div className={styles.content}>
            <div dangerouslySetInnerHTML={{__html: data.rendered_body}} />
          </div>
          {isTopicOrCollection && (
            <div className={[styles.published_in, styles[`${theme}`]].join(" ")}>
              <span>Published in </span>
              {data.topics && data.topics.length > 0 && (
                <span className="topics"><span>topics: &nbsp; </span>
                  {data.topics.map((item, i) => <a key={i} href={item.topic.url} className={styles.tag}>{item.topic.title}</a>)}
                </span>
              )}
              {data.collections && data.collections.length > 0 && (
                <span className={styles.tag_wrapper}><span> &nbsp; and collections: </span>
                  {data.collections.map((item, i) => <a key={i} href={item.collection.url} className={styles.tag}>{item.collection.title}</a>)}
                </span>
              )}
            </div>
          )}

          <ShareTo page={data} services={['facebook', 'twitter', 'linkedin', 'link']} />
          {relatedTopicStories && (<Related title="Related Stories" items={relatedTopicStories.items} />)}
          {latestStories && (<Related title="Latest Stories" items={latestStories.items} moreLink="/stories/" />)}
        </main>
        <div className={styles.sidebar_right}>
          <div className={styles.sidebar_right_wrapper}>
            {featuredStories && (<Related title="Featured Stories" items={featuredStories.items} aside={true} hideMobile={true} /> )}
          </div>
        </div>
      </div>
    </div>
  )
}
