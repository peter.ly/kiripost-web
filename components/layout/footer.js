import Link from 'next/link'
import Copyright from '../content/copyright'
import MenuItem from './menu-item-footer'
import styles from '../../styles/footer.module.scss'
import { footer } from '../../constants/menu'
import LogoKiripost from '../assets/logo-header'
import IconFacebook from '../assets/icon-facebook'
import IconTwitter from '../assets/icon-twitter'
import IconLinkedIn from '../assets/icon-linkedin'
import { social_media } from '../../constants/setting'

export default function Footer({ topics, theme }) {
  return (
    <footer className={[styles.footer, styles[`${theme}`]].join(" ")}>
      <div className={[styles.footer_top, styles[`${theme}`]].join(" ")}>
        <div className={styles.footer_wrapper}>
          <div className={styles.footer_menus}>
            <div className={[styles.footer_col, styles.footer_logo].join(" ")}>
              <Link href="/" title="KiriPost Logo">
                <a><LogoKiripost /></a>
              </Link>
            </div>
            <div className={styles.footer_col}>
              <h3 className={styles.footer_menu_title}>Discover</h3>
              <ul className={[styles.footer_menu_list, styles[`${theme}`]].join(" ")}>
                {footer.discover.map((item, index) => {
                  return <li className={[styles.footer_menu_list_item, styles[`${theme}`]].join(" ")} key={index}><MenuItem key={`footer-menu-discover-item-${index}`} item={item} /></li>
                })}
              </ul>
            </div>
            <div className={styles.footer_col}>
              <h3 className={styles.footer_menu_title}>Topics</h3>
              <ul className={[styles.footer_menu_list, styles[`${theme}`]].join(" ")}>
              {topics.items.map((item, index) => {
                return <li className={[styles.footer_menu_list_item, styles[`${theme}`]].join(" ")} key={index}><MenuItem key={`footer-menu-topics-item-${index}`} item={{uri: item.url, title: item.title}} /></li>
              })}
              </ul>
            </div>
            <div className={styles.footer_col}>
              <h3 className={styles.footer_menu_title}>More</h3>
              <ul className={[styles.footer_menu_list, styles[`${theme}`]].join(" ")}>
              {footer.more.map((item, index) => {
                return <li className={[styles.footer_menu_list_item, styles[`${theme}`]].join(" ")} key={index}><MenuItem key={`footer-menu-more-item-${index}`} item={item} /></li>
              })}
              </ul>
            </div>
          </div>
          <div className={[styles.footer_social, styles.footer_col].join(" ")}>
            <div className={styles.footer_social_wrapper}>
              <h3 className={styles.footer_menu_title}>On Social Media</h3>
              <ul className={[styles.footer_menu_social, styles[`${theme}`]].join(" ")}>
                <li className={[styles.sm_icon, styles.facebook, styles[`${theme}`]].join(" ")}>
                  <Link href={social_media.facebook.url} title={social_media.facebook.title}><a target="_blank"><IconFacebook /></a></Link>
                </li>
                <li className={[styles.footer_menu_list_item, styles.sm_icon, styles.inline, styles.twitter, styles[`${theme}`]].join(" ")}>
                  <Link href={social_media.twitter.url} title={social_media.twitter.title} ><a target="_blank"><IconTwitter /></a></Link>
                </li>
                <li className={[styles.footer_menu_list_item, styles.sm_icon, styles.inline, styles.linkedin, styles[`${theme}`]].join(" ")}>
                  <Link href={social_media.linkedin.url} title={social_media.linkedin.title} ><a target="_blank"><IconLinkedIn /></a></Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div className={[styles.footer_bottom, styles[`${theme}`]].join(" ")}>
        <div className={styles.footer_bottom_wrapper}>
          <Copyright className={styles.copyrights} />
          <div className={styles.terms}>
            <Link href="/terms/">
              <a>Terms of Use</a>
            </Link>
            <Link href="/privacy/">
              <a>Privacy Policy</a>
            </Link>
          </div>
         </div>
      </div>
    </footer>
  )
}