import styles from '../../styles/drawer.module.scss'
import DrawerMenuItem from './drawer-menu-item'
import _ from 'lodash'

export default function DrawerMenu({ title, items, children, theme }) {
  const data = _.get(items, 'items', items)
  return (
    <div className={[styles.drawer_menu, styles[`${theme}`]].join(" ")}>
      {title && <h3 className={styles.title}>{title}</h3>}
      {data
        ? (
          <ul className={styles.menu_items}>
            {data.map((item, index) => {
              return (
                <DrawerMenuItem
                  key={`item-${index}`}
                  item={{
                    name: item.title,
                    uri: _.get(item, 'uri', item.url),
                    intro: _.get(item, 'intro', '')
                  }}
                  className={styles.menu_item}
                  theme={theme}
                  />
              )
            })}
          </ul>
        )
        : <></>
      }
      <div>
        {children}
      </div>
    </div>
  )
}