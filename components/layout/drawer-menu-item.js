import Link from 'next/link'
import styles from '../../styles/drawer.module.scss'
import _ from 'lodash'
import IconArrowRight from '../assets/icon-arrow-right'

export default function DrawerMenuItem({ item, theme }) {
  return (
    <li className={[styles.item, styles[`${theme}`]].join(" ")}>
      <Link href={item.uri}>
        <a className={[styles.item_link, styles[`${theme}`]].join(" ")}>
          <span className={styles.item_title_group}>
            <span className={styles.item_title}>{item.name}</span>
            {item.intro && <span className={styles.item_desc}>{_.truncate(item.intro, {length: 35})}</span>}
          </span>
          <span className={styles.item_icon}><IconArrowRight /></span>
        </a>
      </Link>
    </li>
  )
}
