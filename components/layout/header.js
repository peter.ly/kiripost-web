import Head from './head'
import Menu from './menu'
import Link from 'next/link'
import styles from '../../styles/header.module.scss'
import { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { shortDateFormat } from '../../library/date'
import { site } from '../../constants/setting'

export default function Header({ title, description, image, children, noLogo = false, pageData }) {
  const [openMenu, setOpenMenu] = useState(false)

  return (
    <>
      <Head title={title} description={description} image={image}>
        {children}
      </Head>
      <div className={styles.header}>
        <div className={styles.wrapper}>
          <div>
            <FontAwesomeIcon
              className={styles.burger_menu}
              width="20"
              icon={faBars}
              onClick={() => setOpenMenu(true)}
              />
          </div>
          <div className={styles.logo}>
            {noLogo ? <></> : (
              <Link href="/">
                <a>{site.name}</a>
              </Link>
            )}
          </div>
          <div>{shortDateFormat()}</div>
        </div>
        <Menu className={styles.main_menu} open={openMenu} setOpen={setOpenMenu} pageData={pageData} />
      </div>
    </>
  )
}
