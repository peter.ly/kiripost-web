import Link from 'next/link'

export default function Menu({ item }) {
  return (
    <Link href={item.uri}>
      <a>{item.title}</a>
    </Link>
  )
}
