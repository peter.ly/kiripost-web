import { Fragment } from 'react';
import styles from '../../styles/drawer.module.scss'
import Copyright from '../content/copyright';
import DrawerMenu from './drawer-menu'
import Link from 'next/link'
import _ from 'lodash'
import { main } from '../../constants/menu';
import LogoKiripost from '../assets/logo-kiripost'
import IconX from '../assets/icon-x'
import IconFacebook from '../assets/icon-facebook'
import IconTwitter from '../assets/icon-twitter'
import IconLinkedIn from '../assets/icon-linkedin'
import { social_media } from '../../constants/setting';

export default function Drawer({ open, setOpen, pageData, theme }) {
  const topics = _.get(pageData, 'topics', [])
  const collections = _.get(pageData, 'collections', [])
  const opened = open ? 'opened' : '';
  return (<Fragment>
    <nav className={[styles.drawer, styles[`${theme}`], styles[`${opened}`]].join(" ")}>
      <div className={styles.wrapper}>
        <div className={[styles.header, styles[`${theme}`]].join(" ")}>
          <div className="col">
            <Link href="/">
              <a className={[styles.logo, styles[`${theme}`]].join(" ")}><LogoKiripost /></a>
            </Link>
          </div>
          <div className="col">
            <a href="#" className={[styles.close, styles[`${theme}`]].join(" ")} onClick={() => setOpen(false)}>
              <IconX />
            </a>
          </div>
        </div>
        <div className={styles.content}>
          <div className={styles.content_wrapper}>
            <DrawerMenu title="Discover" items={main.discover} theme={theme} />
            {(topics && topics.meta.total_count > 0) && <DrawerMenu
              title="Topics"
              items={topics}
              theme={theme}
              />}
            {(collections && collections.meta.total_count > 0) && <DrawerMenu
              title="Collections"
              items={collections}
              theme={theme}
              />}
            <DrawerMenu title="More" items={main.more} theme={theme} />
            <DrawerMenu title="Follow us" theme={theme}>
              <div className={styles.drawer_menu}>
                <ul className={[styles.menu_items, styles.inline].join(" ")}>
                    <li className={[styles.sm_icon, styles.facebook, styles[`${theme}`]].join(" ")}>
                      <Link href={social_media.facebook.url} title={social_media.facebook.title}><a target="_blank"><IconFacebook /></a></Link>
                    </li>
                    <li className={[styles.sm_icon, styles.twitter, styles[`${theme}`]].join(" ")}>
                      <Link href={social_media.twitter.url} title={social_media.twitter.title}><a target="_blank"><IconTwitter /></a></Link>
                    </li>
                    <li className={[styles.sm_icon, styles.linkedin, styles[`${theme}`]].join(" ")}>
                      <Link href={social_media.linkedin.url} title={social_media.linkedin.title}><a target="_blank"><IconLinkedIn /></a></Link>
                    </li>
                  </ul>
              </div>
            </DrawerMenu>
            <div className={[styles.copyright, styles[`${theme}`]].join(" ")}>
              <Copyright />
            </div>
          </div>
        </div>
      </div>
    </nav>
    <div className={[styles.backdrop, styles[`${theme}`], styles[`${opened}`]].join(" ")} onClick={() => setOpen(false)}> </div>
    </Fragment>
  )
}
