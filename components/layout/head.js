import NextHead from 'next/head'
import { site } from '../../constants/setting'

export default function Head({ title, description, image, children }) {
  return (
    <NextHead>
      <title>{title} | {site.name}</title>
      {/* facebook metatag */}
      <meta name="facebook-domain-verification" content="7mbb14j1dyv6q2d6fpmm4k38rri2us" />
      
      <meta name="description" content={description} />
      <link rel="icon" href="/favicon.ico" />
      <meta name="robots" content="follow, index" />
      <meta name="description" content={description} />
      <meta property="og:site_name" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:title" content={title} />
      <meta property="og:image" content={image} />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content={site.name} />
      <meta name="twitter:title" content={title} />
      <meta name="twitter:description" content={description} />
      <meta name="twitter:image" content={image} />
      <meta name="theme-color" content="#0082F0" />
      <link rel="preconnect" href="https://fonts.googleapis.com" />
      <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="true" />
      <link href="https://fonts.googleapis.com/css2?family=Bitter:wght@400;700;900&display=swap" rel="stylesheet" />
      {children}
    </NextHead>
  )
}
