import Link from 'next/link'
import Head from 'next/head'
import LogoKiripost from '../assets/logo-header'
import styles from '../../styles/page-error.module.scss'

export default function PageNotFound() {
  const title = 'Error 404 | Page not found'
  return (
    <div className={styles.page_not_found}>
      <Head>
        <title>{title}</title>
        <link href="https://fonts.googleapis.com/css2?family=Bitter:wght@400;700;900&display=swap" rel="stylesheet" />
      </Head>
      <div className={styles.page}>
        <div className={styles.wrapper}>
          <div className={styles.header}>
            <LogoKiripost />
          </div>
          <div className={styles.card}>
            <div className={styles.card_header}><h1 className={styles.title}>{title}</h1></div>
            <div className={styles.card_body}><div className={styles.desc}>The page you are current trying to access cannot be found. Please check the url to make sure it is correct.</div></div>
            <div className={styles.card_footer}><Link href="/"><a className={styles.link}>Go to Home Page</a></Link></div>
            <span className={styles.card_bg}></span>
          </div>
        </div>
      </div>
    </div>
  )
}