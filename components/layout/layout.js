import { useContext } from 'react'
import _ from 'lodash'
import Head from './head'
import Footer from './footer'
import styles from '../../styles/layout.module.scss'
import ThemeContext from '../../constants/theme-context';
import Topbar from './topbar'
import { getContentData } from '../../library/pages'

export default function Layout({ children, content, pageData, abovePage=null }) {
  const { theme } = useContext(ThemeContext)
  const title = getContentData(content, 'meta.seo_title', 'title')
  const desc = getContentData(content, 'meta.search_description', 'intro')
  const cover = _.get(content,'cover.url', 'https://cdn.kiripost.com/images/share-image.original.png')
  return (
    <div className={[styles.container, styles[`${theme}`]].join(" ")}>
      <Head title={title} description={_.truncate(desc, {length: 200})} image={cover} />
      <Topbar pageData={pageData} />
      <div className={styles.main}>
        {abovePage && <div className={[styles.above_page_section, styles[`${theme}`]].join(" ")}>{abovePage}</div>}
        <div className={styles.page}>{children}</div>
      </div>
      <Footer topics={pageData.topics} theme={theme} />
    </div>
  )
}
