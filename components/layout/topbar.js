import Drawer from './drawer'
import Link from 'next/link'
import styles from '../../styles/header.module.scss'
import { useState, useEffect, useContext, Fragment } from 'react'
import IconTheme from '../assets/icon-theme'
import IconBurger from '../assets/icon-burger'
import LogoKiripost from '../assets/logo-header'
import ThemeContext from '../../constants/theme-context';

export default function Topbar ({ pageData }) {
  const [openMenu, setOpenMenu] = useState(false)
  const [scrolled, setScrolled] = useState(false)
  const [scrolledUp, setScrolledUp] = useState(false)
  const {theme} = useContext(ThemeContext)
  let topbarClasses = [styles.topbar, styles[`topbar_${theme}`]]
  let lastScrollTop = null

  const handleScroll = () => {
    const offset = window.scrollY;
    const st = window.pageYOffset || document.documentElement.scrollTop;
    const isScroll = offset > 200
    setScrolled(isScroll)
    if (isScroll && lastScrollTop !== null) {
      if (!scrolledUp && st < lastScrollTop) setScrolledUp(true)
      else if (scrolledUp && st > lastScrollTop) setScrolledUp(false)
    }
    lastScrollTop = st;
  }

  useEffect(() => {
    window.addEventListener('scroll', handleScroll)
  })

  if (scrolled) {
    topbarClasses.push(styles.scrolled)
    if (scrolledUp) topbarClasses.push(styles.scrolledUp)
  }

  return (
    <Fragment>
      <Drawer className={styles.main_menu} open={openMenu} setOpen={setOpenMenu} pageData={pageData} theme={theme} />
      <header className={topbarClasses.join(" ")}>
        <div className={styles.bar_wrapper}>
          <div className={styles.bar_column}>
            <IconBurger onClick={() => setOpenMenu(true)} />
          </div>
          <div className={styles.bar_column}>
            <Link href="/" title="KiriPost">
              <a><LogoKiripost /></a>
            </Link>
          </div>
          <div className={styles.bar_column}><IconTheme /></div>
        </div>
      </header>
    </Fragment>
  )
}
