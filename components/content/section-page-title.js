import styles from '../../styles/section.module.scss'

export default function SectionPageTitle ({ titleData }) {
  return (
    <div className={[styles.section_title, styles['fit']].join(" ")}>
      <div className={styles.title_group}>
        <h1 className={styles.title}>{titleData.title}</h1>
        {titleData.desc && <div className={styles.desc}>{titleData.desc}</div>}
      </div>
      <div className={styles.meta}>
        <span className={styles.count_number}>{titleData.total_item_count}</span>
        <div className={styles.count_label}>Item count</div>
      </div>
    </div>
  )
}
