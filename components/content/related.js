import React, { useContext } from 'react';
import styles from '../../styles/related.module.scss'
import ThemeContext from '../../constants/theme-context';
import Cards from './cards';

export default function Related ({ title, items, grid='grid-3', moreLink=null, featured=false, aside=false, hideMobile=false }) {
  const { theme } = useContext(ThemeContext);
  const wrapper_styles = hideMobile ? [styles.wrapper, styles.hide_mobile].join(" ") : styles.wrapper;
  const html = <div className={wrapper_styles}>
    {title && <div className={styles.title_group}>
      <h2 className={styles.title}>{title}</h2>
      {moreLink && (<a className={styles.more_link} href={moreLink} title="More Stories">More</a>)}
    </div>}
    <div className={styles.children}><Cards items={items} aside={aside} grid={grid} featured={featured} /></div>
  </div>
  return items.length > 0 ? html : "";
}