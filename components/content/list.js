import styles from '../../styles/list.module.scss'

export default function List({ children }) {
  return (
    <div className={styles.main}>
      {children}
    </div>
  )
}
