import ProfileImage from '../../components/content/profile-image'
import styles from '../../styles/authors.module.scss'
import { longDateFormat, shorterDateFormat } from '../../library/date'

export default function Authors({ title, imageSrc, authors, date, isCard}) {
  const fullName = `${authors[0].username}`
  if (isCard) {
    return (
      <div className={styles.author_short}>
        <div>
          <ProfileImage alt={title} src={imageSrc} />
          <span>
            By: <strong>{fullName}</strong>
          </span>
        </div>
        <span>{shorterDateFormat(date)}</span>
      </div>
    )
  }
  return (
    <div className={styles.author}>
      <ProfileImage alt={title} src={imageSrc} />
      <span>
        By: <strong>{fullName}</strong>
      </span>
      <span>{longDateFormat(date)}</span>
    </div>
  )
}
