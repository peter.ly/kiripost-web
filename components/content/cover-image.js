import styles from '../../styles/content.module.scss'
import { asset } from '../../library/request'

export default function CoverImage({ alt, src}) {
  return (
    <div className={styles.cover_image}>
      {/* <span className={styles.highlight}> */}
        <img
          alt={alt}
          src={asset(src)}
          />
      {/* </span> */}
      <p className={styles.image_caption}>{alt}</p>
    </div>
  )
}
