import styles from '../../styles/authors.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTwitterSquare, faFacebookSquare, faLinkedin } from '@fortawesome/free-brands-svg-icons'

export default function SocialIcons() {
  return (
    <div className={styles.social}>
      <div className={styles.social_media}>
        <FontAwesomeIcon icon={faFacebookSquare} width="20" />
        <FontAwesomeIcon icon={faTwitterSquare} width="20" />
        <FontAwesomeIcon icon={faLinkedin} width="20" />
      </div>
    </div>
  )
}
