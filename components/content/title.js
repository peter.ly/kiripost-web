import styles from '../../styles/content.module.scss'

export default function Title({ children }) {
  return (
    <h1 className={styles.title}>
      <span className={styles.highlight}>
        {children}
      </span>
    </h1>
  )
}
