import styles from '../../styles/authors.module.scss'
import { asset } from '../../library/request'

export default function ProfileImage({ alt, src}) {
  if (! src) {
    return <></>
  }
  return <img className={styles.profile_image} alt={alt} src={asset(src)} />
}
