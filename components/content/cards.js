import React, { useContext } from 'react';
import cardStyles from '../../styles/cards.module.scss'
import sidebarCardStyles from '../../styles/sidebar-cards.module.scss'
import ThemeContext from '../../constants/theme-context';

export default function Cards ({ items, grid='grid-3', featured=false, aside=false }) {
  const { theme } = useContext(ThemeContext)
  const styles = aside ? sidebarCardStyles : cardStyles;
  const featuredClass = featured ? "featured" : "";
  return <ul className={[styles.cards, styles[`${theme}`]].join(" ")}>
    {items.map((item, index) => {
      return (<li className={[styles.item, styles[`${grid}`], styles[`${featuredClass}`], styles[`${theme}`]].join(" ")} key={index}>
          <div className={styles.wrapper}>
            <div className={[styles.card, styles[`${theme}`]].join(" ")}>
              {item.is_sponsored && (<span className={styles.is_sponsored}>Sponsored</span>)}
              <div className={styles.item_image}>
                <img src={item.cover_portrait_thumbnail.url} width={item.cover_portrait_thumbnail.width} height={item.cover_portrait_thumbnail.height} alt={item.cover_portrait_thumbnail.alt} />
              </div>
              <div className={styles.item_title}>
                <a className={styles.title_link} href={item.url}>
                  <span className={styles.title_content_group}>
                    {item.alt_title && <span className={[styles.alt_title, styles[`${theme}`]].join(" ")}>{item.alt_title}</span>}
                    <span className={[styles.title, styles[`${theme}`]].join(" ")}>{item.title}</span>
                  </span>
                </a>
              </div>
              <div className={styles.title_shade}></div>
            </div>
          </div>
        </li>)
      })
    }
  </ul>
}