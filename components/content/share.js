import React, { useContext, useState } from 'react';
import styles from '../../styles/share.module.scss'
import ThemeContext from '../../constants/theme-context';
import IconFacebook from '../assets/icon-facebook'
import IconTwitter from '../assets/icon-twitter'
import IconLinkedIn from '../assets/icon-linkedin'
import IconLink from '../assets/icon-link'

export default function ShareTo ({ page, services, orientation='horizontal' }) {
  const { theme } = useContext(ThemeContext);
  const [isCopied, setIsCopied] = useState(false);
  const showIsCopied = () => {
    setIsCopied(true);
    setTimeout(() => {
      setIsCopied(false);
    }, 2000);
  }
  const handleShareClick = (e, site) => {
    e.preventDefault();
    const url = encodeURIComponent(page.meta.html_url);
    const title = encodeURIComponent(page.title);
    let shareUrl = '';
    if (site === 'facebook') {
      shareUrl = `https://www.facebook.com/sharer.php?u=${url}`;
    } else if (site === 'twitter') {
      shareUrl = `http://twitter.com/share?text=${title}&url=${url}&counturl=${url}&via=kiripost`;
    } else if (site === 'linkedin') {
      shareUrl = `https://www.linkedin.com/shareArticle?url=${url}&title=${title}&mini=true`;
    } else if (site === 'link') {
      navigator.clipboard.writeText(url);
      showIsCopied();
    }
    if (site !== 'link') {
      window.open(shareUrl, '', 'toolbar=0,status=0,scrollbars=1,width=626,height=436');
    }
  }
  return (
    <div className={[styles.share_wrapper, styles[`${orientation}`]].join(" ")}>
      <span className={styles.title}>Share to</span>
      <div className={[styles.button_group, styles[`${orientation}`]].join(" ")}>
        {services.includes('facebook') && (
        <div className={styles.button_wrapper}>
          <a href="#" className={[styles.button, styles[`${theme}`]].join(" ")} onClick={(e) => handleShareClick(e, 'facebook')}>
            <IconFacebook />
          </a>
        </div>
        )}
        {services.includes('twitter') && (
        <div className={styles.button_wrapper}>
          <a href="#" className={[styles.button, styles[`${theme}`]].join(" ")} onClick={(e) => handleShareClick(e, 'twitter')}>
            <IconTwitter />
          </a>
        </div>
        )}
        {services.includes('linkedin') && (
        <div className={styles.button_wrapper}>
          <a href="#" className={[styles.button, styles[`${theme}`]].join(" ")} onClick={(e) => handleShareClick(e, 'linkedin')}>
            <IconLinkedIn />
          </a>
        </div>
        )}
        {services.includes('link') && (
        <div className={styles.button_wrapper}>
          <a href="#" className={[styles.button, styles[`${theme}`]].join(" ")} onClick={(e) => handleShareClick(e, 'link')}>
            <IconLink />
          </a>
          {isCopied && (<span className={styles.copied}>Link is copied.</span>)}
        </div>
        )}
      </div>
    </div>
  )
}
