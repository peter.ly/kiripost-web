import Link from 'next/link'
import styles from '../../styles/pagination.module.scss'
import _ from 'lodash'
import { pagination } from '../../constants/setting'
import { getPages } from '../../library/pagination'

export default function Pagination({ page = 1, count, link }) {
  const  total = Math.ceil(_.parseInt(count) / pagination.limit)
  const current = _.parseInt(page)

  if (total <= 1 || current > total) {
    return <></>
  }

  const pages = getPages(current, total)
  const prefix = `/${_.trim(link, '/')}/page`

  return (
    <div className={styles.pagination}>
      <ul>
        {current > 1
          ? <li><Link href={`${prefix}/${current - 1}`}><a>Previous</a></Link></li>
          : <></>}

        {pages.map((page, index) => {
          return _.isInteger(page)
            ? (
            <li key={`page-${index}`} className={current == page ? styles.current : ''}>
              <Link href={`${prefix}/${page}`}>
                <a>{page}</a>
              </Link>
            </li>
          )
          : (<li><a>{page}</a></li>)
        })}

        {current < total
          ? <li><Link href={`${prefix}/${current + 1}`}><a>Next</a></Link></li>
          : <></>}

      </ul>
    </div>
  )
}
