import moment from 'moment'

export default function Copyright({ className }) {
  return (
    <span className={className}>
      &#169; {moment().format('YYYY')} Kiripost. All Rights Reserved.
    </span>
  )
}
