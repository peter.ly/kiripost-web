const discover = [
  {
    title: 'Topics',
    uri: '/topics/',
    intro: 'Discover stories by topics'
    },
  {
    title: 'Collections',
    uri: '/collections/',
    intro: 'Discover stories by collections'
    },
  {
    title: 'Latest',
    uri: '/stories/',
    intro: 'Most recent stories ordered by date published'
    },
  {
    title: 'Featured',
    uri: '/stories/featured/',
    intro: 'Most important stories you need to see'
    },
  {
    title: 'Sponsored',
    uri: '/stories/sponsored/',
    intro: 'Discover stories that are sponsored by our partners.'
    }
]
const more = [
  {
    title: 'About us',
    uri: '/about/',
    intro: 'Lean more about the company and team behind.'
  },
  {
    title: 'Contact us',
    uri: '/contact/',
    intro: 'Reach out to the team for inquiries.'
  },
  {
    title: 'Sponsorship',
    uri: '/sponsorship/',
    intro: 'Find out how you could become our sponsor too.'
  }
]
const main = {
  discover: discover,
  more: more
}

const footer = {
  discover: discover,
  more: more
}

export {
  footer,
  main
}