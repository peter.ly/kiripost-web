const api = {
  story: {
    list: `?type=blog.Post&order=-published_at&fields=cover_portrait_thumbnail,authors(username),topics,url,alt_title,is_sponsored`,
    detail: '?type=blog.Post&fields=published_at,authors(username,first_name,last_name,intro),cover,cover_thumbnail,cover_portrait,cover_portrait_thumbnail,rendered_body,readtime,intro,topics',
  },
  topic: {
    list: `?type=blog.Topic&fields=cover_portrait_thumbnail,url`,
    detail: '?type=blog.Topic&fields=published_at,rendered_body,intro'
  },
  collection: {
    list: `?type=blog.Collection&fields=intro,cover_portrait_thumbnail,url`,
    detail: '?type=blog.Collection&fields=published_at,rendered_body,intro'
  },
  pages: {
    about: 'about',
    contact: 'contact',
    collections: 'collections',
    topics: 'topics',
    stories: 'stories',
    terms: 'terms',
    sponsorship: 'sponsorship',
    privacy: 'privacy',
  }
}

export default api
