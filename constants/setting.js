const site = {
  name: 'Kiripost',
  description: 'Kiripost is a digital media company covering Cambodia’s startups and businesses. We bring our readers news, insights, and in-depth stories from entrepreneurs, inventors, innovators, and makers. ',
  title: 'Business & Technology News Stories from Cambodia, Asia',
  gtag_id: 'G-0WR621JH5E'
}

const social_media = {
  linkedin: {
    url: 'https://www.linkedin.com/company/kiripost/',
    title: 'Kiripost LinkedIn page'
  },
  facebook: {
    url: 'https://facebook.com/kiripostmedia',
    title: 'Kiripost Facebook page'
  },
  twitter: {
    url: 'https://twitter.com/kiripost',
    title: 'Kiripost Twitter page'
  }
}

const base_url = process.env.API_HOST
const asset_url = ''
const api_base_url = base_url + '/api/v2/pages/'

const types = {
  collection: 'collections',
  featured: 'featured',
  sponsored: 'sponsored',
  story: 'stories',
  topic: 'topics'
}

const pagination = {
  limit: 48
}

const revalidation_time = 86400 // seconds (1day)

export {
  asset_url,
  api_base_url,
  base_url,
  pagination,
  site,
  social_media,
  types,
  revalidation_time
}