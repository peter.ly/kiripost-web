import Layout from '../../components/layout/layout'
import SectionPageTitle from '../../components/content/section-page-title'
import { getStories, getStoryIndex } from '../../library/stories'
import { getContentData, getPageData } from '../../library/pages'
import styles from '../../styles/section.module.scss';
import Related from '../../components/content/related';
import Pagination from '../../components/content/pagination';

export default function StoryList ({ storyIndex, storyList, pageData }) {
  const content = {
    title: "Stories",
    intro: getContentData(storyIndex, 'search_description', 'intro')
  }

  const titleData = {
    "title": content.title,
    "desc": content.intro,
    "total_item_count": storyList.meta.total_count
  }
  const abovePage = (<SectionPageTitle titleData={titleData} />);

  return (
    <Layout content={content} pageData={pageData} abovePage={abovePage}>
        {storyList && (
          <>
            <Related items={storyList.items} grid="grid-4" />
            <Pagination count={storyList.meta.total_count} link={storyIndex.url} />
          </>
          )}
    </Layout>
  )
}

export async function getServerSideProps() {
  const storyIndex = await getStoryIndex()
  const storyList = await getStories(true)
  const pageData = await getPageData()

  return {
    props: { storyIndex, storyList, pageData },
  }
}