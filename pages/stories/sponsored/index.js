import Layout from '../../../components/layout/layout'
import SectionPageTitle from '../../../components/content/section-page-title'
import { getSponsoredStories, getStoryIndex } from '../../../library/stories'
import { getPageData, getContentData } from '../../../library/pages'
import Related from '../../../components/content/related';
import styles from '../../../styles/section.module.scss';
import Pagination from '../../../components/content/pagination';
import { types } from '../../../constants/setting';

export default function SponsoredStoryList({ storyIndex, storyList, pageData }) {
  const content = {
    title: "Sponsored Stories",
    intro: getContentData(storyIndex, 'search_description', 'intro')
  }
  const titleData = {
    "title": content.title,
    "desc": content.intro,
    "total_item_count": storyList.meta.total_count
  }
  const abovePage = (<SectionPageTitle titleData={titleData} />);

  return (
    <Layout content={content} pageData={pageData} abovePage={abovePage}>
        {storyList && (
          <>
            <Related items={storyList.items} grid="grid-4" />
            <Pagination count={storyList.meta.total_count} link={`${storyIndex.url}${types.sponsored}`} />
          </>
          )}
    </Layout>
  )
}

export async function getServerSideProps() {
  const storyIndex = await getStoryIndex()
  const storyList = await getSponsoredStories(true)
  const pageData = await getPageData()

  return {
    props: { storyList, storyIndex, pageData },
  }
}