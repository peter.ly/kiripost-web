import Layout from '../../components/layout/layout'
import Detail from '../../components/story/detail'
import { getStories, getStoryDetail, getLatestStories, getStoryByTopics, getFeaturedStories } from '../../library/stories'
import { getPageData } from '../../library/pages'
import { revalidation_time } from '../../constants/setting'

export default function StoryDetail ({ story, pageData, latestStories, topicStories, featuredStories }) {
  return (
    <Layout content={story} pageData={pageData}>
      <Detail data={story} latestStories={latestStories} relatedTopicStories={topicStories} featuredStories={featuredStories}  />
    </Layout>
  )
}

// export async function getStaticPaths() {
//   const data = await getStories()

//   const paths = data.items.map((story) => ({
//     params: { slug: story.meta.slug },
//   }))

//   return { paths, fallback: 'blocking' }
// }

export async function getServerSideProps({ params }) {
  const story = await getStoryDetail(params.slug)
  const topicIds = []
  story.topics.forEach(element => {
    topicIds.push(element.topic.id);
  });
  const latestStories = await getLatestStories();
  const featuredStories = await getFeaturedStories();
  let topicStories = null;
  if (topicIds.length > 0) {
    topicStories = await getStoryByTopics(topicIds);
  }
  const pageData = await getPageData()

  return {
    props: { story, pageData, latestStories, topicStories, featuredStories },
    // revalidate: revalidation_time
  }
}