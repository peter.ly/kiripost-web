import Layout from '../../../../components/layout/layout'
import SectionPageTitle from '../../../../components/content/section-page-title'
import { types } from '../../../../constants/setting'
import { getFeaturedStories, getStoryIndex } from '../../../../library/stories'
import { getPaginatedPaths } from '../../../../library/pagination'
import { getPageData, getContentData } from '../../../../library/pages'
import Related from '../../../../components/content/related'
import Pagination from '../../../../components/content/pagination'
import { revalidation_time } from '../../../../constants/setting'

export default function FeaturedStoryList({ page, storyIndex, storyList, pageData }) {
  const content = {
    title: "Featured Stories",
    intro: getContentData(storyIndex, 'search_description', 'intro')
  }
  const titleData = {
    "title": content.title,
    "desc": content.intro,
    "total_item_count": storyList.meta.total_count
  }
  const abovePage = (<SectionPageTitle titleData={titleData} />);

  return (
    <Layout content={content} pageData={pageData} abovePage={abovePage}>
      <Related items={storyList.items} grid="grid-4" />
      <Pagination count={storyList.meta.total_count} link={`${storyIndex.url}${types.featured}`} page={page} />
    </Layout>
  )
}

// export async function getStaticPaths() {
//   const paths = getPaginatedPaths(await getFeaturedStories())
//   return { paths, fallback: 'blocking' }
// }

export async function getServerSideProps({ params: { page } }) {
  const storyList = await getFeaturedStories(true, page)
  const storyIndex = await getStoryIndex()
  const pageData = await getPageData()

  return {
    props: { page, storyIndex, storyList, pageData },
    // revalidate: revalidation_time
  }
}