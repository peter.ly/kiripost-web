import PageNotFound from '../components/layout/page-not-found'

export default function Error404() {
  return <PageNotFound />
}
