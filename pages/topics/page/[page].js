import Layout from '../../../components/layout/layout'
import SectionPageTitle from '../../../components/content/section-page-title'
import Cards from '../../../components/content/cards.js'
import { getTopics, getTopicIndex } from '../../../library/topics'
import { getPaginatedPaths } from '../../../library/pagination'
import { getPageData } from '../../../library/pages'
import Pagination from '../../../components/content/pagination'
import { revalidation_time } from '../../../constants/setting'

export default function TopicList({ topicIndex, page, topicList, pageData }) {
  const titleData = {
    "title": topicIndex.title,
    "desc": topicIndex.intro,
    "total_item_count": pageData.topics.meta.total_count
  }
  const abovePage = (<SectionPageTitle titleData={titleData} />);
  return (
    <Layout content={topicIndex} pageData={pageData} abovePage={abovePage}>
      <Cards items={topicList.items} grid="grid-6" />
      <Pagination count={topicList.meta.total_count} link={topicIndex.url} page={page} />
    </Layout>
  )
}

// export async function getStaticPaths() {
//   const paths = getPaginatedPaths(await getTopics())

//   return { paths, fallback: 'blocking' }
// }

export async function getServerSideProps({ params: { page } }) {
  const topicList = await getTopics(true, page)
  const pageData = await getPageData()
  const topicIndex = await getTopicIndex()

  return {
    props: { page, topicList, topicIndex, pageData },
    // revalidate: revalidation_time
  }
}