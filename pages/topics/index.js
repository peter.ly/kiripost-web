import Layout from '../../components/layout/layout'
import SectionPageTitle from '../../components/content/section-page-title'
import Cards from '../../components/content/cards.js'
import { getTopics, getTopicIndex } from '../../library/topics'
import { getPageData } from '../../library/pages'
import Pagination from '../../components/content/pagination'

export default function TopicList({ topicIndex, paginatedTopics, pageData }) {
  const titleData = {
    "title": topicIndex.title,
    "desc": topicIndex.intro,
    "total_item_count": pageData.topics.meta.total_count
  }
  const abovePage = (<SectionPageTitle titleData={titleData} />);
  return (
    <Layout content={topicIndex} pageData={pageData} abovePage={abovePage}>
      <Cards items={paginatedTopics.items} grid="grid-6" />
      <Pagination count={paginatedTopics.meta.total_count} link={topicIndex.url} />
    </Layout>
  )
}

export async function getServerSideProps() {
  const paginatedTopics = await getTopics(true)
  const pageData = await getPageData()
  const topicIndex = await getTopicIndex()

  return {
    props: {
      paginatedTopics,
      pageData,
      topicIndex
    },
  }
}