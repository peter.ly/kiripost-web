import SectionPageTitle from '../../../../components/content/section-page-title'
import Layout from '../../../../components/layout/layout'
import { getTopicDetail, getTopicStories, getTopics } from '../../../../library/topics'
import { getPageData } from '../../../../library/pages'
import { getPaginatedPaths } from '../../../../library/pagination'
import Related from '../../../../components/content/related'
import _ from 'lodash'
import Pagination from '../../../../components/content/pagination'
import { revalidation_time } from '../../../../constants/setting'

export default function TopicDetail({ page, topic, topicStories, pageData }) {
  const titleData = {
    "title": topic.title,
    "desc": topic.intro,
    "total_item_count": topicStories.meta.total_count
  }
  const abovePage = (<SectionPageTitle titleData={titleData} />);

  return (
    <Layout content={topic} pageData={pageData} abovePage={abovePage}>
      <Related title="Stories" items={topicStories.items} grid="grid-4" />
      <Pagination count={topicStories.meta.total_count} link={topic.url} page={page} />
    </Layout>
  )
}

// export async function getStaticPaths() {
//   const paths = []
//   const topics = await getTopics()

//   for(let i = 0; i < topics.meta.total_count; i++) {
//     const item = topics.items[i]
//     const storyPaths = getPaginatedPaths(await getTopicStories(item.id), {slug: item.meta.slug})
//     paths.push(...storyPaths)
//   }

//   return { paths, fallback: 'blocking' }
// }

export async function getServerSideProps({ params: { slug, page } }) {
  const topic = await getTopicDetail(slug)
  const topicStories = await getTopicStories(topic.id, true, page)
  const pageData = await getPageData()

  return {
    props: { page, topic, topicStories, pageData },
    // revalidate: revalidation_time
  }
}
