import SectionPageTitle from '../../../components/content/section-page-title'
import Related from '../../../components/content/related'
import Layout from '../../../components/layout/layout'
import { getTopicDetail, getTopicStories, getTopics } from '../../../library/topics'
import { getPageData } from '../../../library/pages'
import Pagination from '../../../components/content/pagination'

export default function TopicDetail({ topic, topicStories, pageData }) {
  const titleData = {
    "title": topic.title,
    "desc": topic.intro,
    "total_item_count": topicStories.meta.total_count
  }
  const abovePage = (<SectionPageTitle titleData={titleData} />);

  return (
    <Layout content={topic} pageData={pageData} abovePage={abovePage}>
      <Related items={topicStories.items} grid="grid-4" />
      <Pagination count={topicStories.meta.total_count} link={topic.url} />
    </Layout>
  )
}

// export async function getStaticPaths() {
//   const topics = await getTopics()
//   const paths = topics.items.map((topic) => ({
//     params: { slug: topic.meta.slug },
//   }))

//   return { paths, fallback: false }
// }

export async function getServerSideProps({ params: { slug } }) {
  const topic = await getTopicDetail(slug)
  const topicStories = await getTopicStories(topic.id, true)
  const pageData = await getPageData()

  return {
    props: { topic, topicStories, pageData },
  }
}
