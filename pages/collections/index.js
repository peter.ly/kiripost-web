import Layout from '../../components/layout/layout'
import SectionPageTitle from '../../components/content/section-page-title'
import styles from '../../styles/section.module.scss'
import { getCollections, getCollectionIndex } from '../../library/collections'
import { getPageData } from '../../library/pages'
import Cards from '../../components/content/cards.js'
import Pagination from '../../components/content/pagination'

export default function CollectionList({ collections, collectionIndex, pageData }) {
  const titleData = {
    "title": collectionIndex.title,
    "desc": collectionIndex.intro,
    "total_item_count": pageData.collections.meta.total_count
  }
  const abovePage = (<SectionPageTitle titleData={titleData} />);
  return (
    <Layout content={collectionIndex} pageData={pageData} abovePage={abovePage}>
      <Cards items={collections.items} grid="grid-5" />
      <Pagination count={collections.meta.total_count} link={collectionIndex.url} />
    </Layout>
  )
}

export async function getServerSideProps() {
  const collections = await getCollections(true)
  const collectionIndex = await getCollectionIndex()
  const pageData = await getPageData()

  return {
    props: { collectionIndex, collections, pageData }
  }
}