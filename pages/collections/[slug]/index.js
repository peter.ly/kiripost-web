import Layout from '../../../components/layout/layout'
import SectionPageTitle from '../../../components/content/section-page-title'
import Related from '../../../components/content/related'
import {
  getCollectionDetail,
  getCollectionStories,
  getCollections
} from '../../../library/collections'
import { getPageData } from '../../../library/pages'
import Pagination from '../../../components/content/pagination'

export default function CollectionDetail({ collection, collectionStories, pageData }) {
  const titleData = {
    "title": collection.title,
    "desc": collection.intro,
    "total_item_count": collectionStories.meta.total_count
  }
  const abovePage = (<SectionPageTitle titleData={titleData} />);

  return (
    <Layout content={collection} pageData={pageData} abovePage={abovePage}>
      <Related items={collectionStories.items} grid="grid-4" />
      <Pagination count={collectionStories.meta.total_count} link={collection.url} />
    </Layout>
  )
}

// export async function getStaticPaths() {
//   const data = await getCollections()

//   const paths = data.items.map((collection) => ({
//     params: { slug: collection.meta.slug },
//   }))

//   return { paths, fallback: false }
// }

export async function getServerSideProps({ params }) {
  const collection = await getCollectionDetail(params.slug)
  const collectionStories = await getCollectionStories(collection.id, true)
  const pageData = await getPageData()

  return {
    props: { collection, collectionStories, pageData },
  }
}
