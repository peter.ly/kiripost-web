import Layout from '../../../../components/layout/layout'
import SectionPageTitle from '../../../../components/content/section-page-title'
import styles from '../../../../styles/page.module.scss'
import {
  getCollectionDetail,
  getCollectionStories,
  getCollections
} from '../../../../library/collections'
import { getPageData } from '../../../../library/pages'
import { getPaginatedPaths } from '../../../../library/pagination'
import Related from '../../../../components/content/related'
import Pagination from '../../../../components/content/pagination'
import { revalidation_time } from '../../../../constants/setting'

export default function CollectionDetail({ collection, collectionStories, pageData, page }) {
  const titleData = {
    "title": collection.title,
    "desc": collection.intro,
    "total_item_count": collectionStories.meta.total_count
  }
  const abovePage = (<SectionPageTitle titleData={titleData} />);

  return (
    <Layout content={collection} pageData={pageData} abovePage={abovePage}>
      <Related title="Stories" items={collectionStories.items} grid="grid-4" />
      <Pagination count={collectionStories.meta.total_count} link={collection.url} page={page} />
    </Layout>
  )
}

// export async function getStaticPaths() {
//   const paths = []
//   const collections = await getCollections()

//   for(let i = 0; i < collections.meta.total_count; i++) {
//     const item = collections.items[i]
//     const storyPaths = getPaginatedPaths(await getCollectionStories(item.id), {slug: item.meta.slug})
//     paths.push(...storyPaths)
//   }

//   return { paths, fallback: 'blocking' }
// }

export async function getServerSideProps({ params: { slug, page } }) {
  const collection = await getCollectionDetail(slug)
  const collectionStories = await getCollectionStories(collection.id, true, page)
  const pageData = await getPageData()

  return {
    props: { collection, collectionStories, pageData, page },
    // revalidate: revalidation_time
  }
}
