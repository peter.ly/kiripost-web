import Layout from '../../../components/layout/layout'
import SectionPageTitle from '../../../components/content/section-page-title'
import styles from '../../../styles/page.module.scss'
import { getCollections, getCollectionIndex } from '../../../library/collections'
import { getPaginatedPaths } from '../../../library/pagination'
import { getPageData } from '../../../library/pages'
import Cards from '../../../components/content/cards'
import Pagination from '../../../components/content/pagination'
import { revalidation_time } from '../../../constants/setting'

export default function CollectionList({ collectionIndex, collections, page, pageData }) {
  const titleData = {
    "title": collectionIndex.title,
    "desc": collectionIndex.intro,
    "total_item_count": pageData.collections.meta.total_count
  }
  const abovePage = (<SectionPageTitle titleData={titleData} />);
  return (
    <Layout content={collectionIndex} pageData={pageData} abovePage={abovePage}>
      <Cards items={collections.items} grid="grid-5" />
      <Pagination count={collections.meta.total_count} link={collectionIndex.url} page={page} />
    </Layout>
  )
}

// export async function getStaticPaths() {
//   const paths = getPaginatedPaths(await getCollections())

//   return { paths, fallback: 'blocking' }
// }

export async function getServerSideProps({ params: { page } }) {
  const collectionIndex = await getCollectionIndex()
  const collections = await getCollections(true, page)
  const pageData = await getPageData()

  return {
    props: {
      collections,
      collectionIndex,
      page,
      pageData
    },
    // revalidate: revalidation_time
  }
}