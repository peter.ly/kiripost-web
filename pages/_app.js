import { useState } from "react";
import ThemeContext from '../constants/theme-context'
import '../styles/globals.scss'
import GTAG from '../components/layout/gtag'

const MyApp = ({ Component, pageProps }) => {
  const [theme, setTheme] = useState("light")
  const value = {theme, setTheme}
  return <ThemeContext.Provider value={value}>
      {process.env.NODE_ENV === 'production' && <GTAG />}
      <Component {...pageProps} />
  </ThemeContext.Provider>
}

export default MyApp
