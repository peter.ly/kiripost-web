// server-sitemap/[page].ts
// route rewritten from /server-sitemap-[page].xml
import { getServerSideSitemapLegacy } from "next-sitemap";
import { getLatestStories } from "../../library/stories";
import moment from "moment";

export const getServerSideProps = async (ctx) => {
  if (!ctx.params?.page || isNaN(Number(ctx.params?.page))) {
    return { notFound: true };
  }

  const page = Number(ctx.params?.page);

  // this would load the items that make dynamic pages
  // console.log("offset: ",page * 500);
  const response = await getLatestStories(false, 0, 500, page * 500);

  if (response?.items?.length === 0) {
    return { notFound: true };
  }

  const fields = response?.items?.map((item) => {
    // !!! this one not working (toISOString)
    const lastmod = moment().format("YYYY-MM-DD");
    // const date = item?.meta?.first_published_at ? moment(item?.meta?.first_published_at).format("YYYY-MM-DDThh:mmTZD") : "";

    // ??? these one working in srr (toISOString)
    const date = item?.meta?.first_published_at
      ? new Date(item?.meta?.first_published_at).toISOString()
      : "";
    const images = [{ loc: item?.cover_portrait_thumbnail?.url || "FF" }];
    return {
      loc: `https://kiripost.com/${item?.url}`,
      lastmod,
      // images:[{loc: "ffff"}],
      news: {
        title: item?.title || "",
        publicationName: "Kiripost",
        publicationLanguage: "en",
        date,
      },
    };
  });

  return getServerSideSitemapLegacy(ctx, fields);
};

// Default export to prevent next.js errors
export default function MemorialSitemapPage() {}
