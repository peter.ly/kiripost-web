import { getServerSideSitemapIndexLegacy } from "next-sitemap";
import { getLatestStories } from "../../library/stories";

const URLS_PER_SITEMAP = 500;

export const getServerSideProps = async (ctx) => {
  // *** get count from API to make dynamic sitemap
  const response = await getLatestStories(false, 0, 0);
  const totalCount = response?.meta?.total_count || 0;

  // *** generate per page item of sitemap
  const totalSitemaps = Math.ceil(totalCount / URLS_PER_SITEMAP);

  const sitemaps = Array(totalSitemaps)
    .fill("")
    .map((_, index) => `https://kiripost.com/server-sitemap-${index}.xml`);

  return getServerSideSitemapIndexLegacy(ctx, sitemaps);
};

// Default export to prevent next.js errors
export default function SitemapIndex() {}
