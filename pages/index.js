import Layout from '../components/layout/layout'
import styles from '../styles/home.module.scss'
import _ from 'lodash'
import { site } from '../constants/setting'
import { getStoriesForHome } from '../library/stories'
import { getPageData } from '../library/pages'
import Related from '../components/content/related';

export default function Home({ stories, pageData }) {
  const featured = _.get(stories, 'featured.items', [])
  const content = {
    title: site.title,
    intro: site.description
  }

  return (
    <Layout content={content} pageData={pageData}>
      <div className={styles.main}>
        {featured && (<Related items={featured} featured={true} grid="grid-4" />)}
        {stories.latest && (<Related title="Latest" items={stories.latest.items} grid="grid-4" moreLink="/stories/" />)}
      </div>
    </Layout>
  )
}

export async function getServerSideProps() {
  const stories = await getStoriesForHome()
  const pageData = await getPageData()

  return {
    props: { stories, pageData },
  }
}
