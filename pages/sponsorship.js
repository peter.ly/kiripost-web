import React, { useContext } from 'react';
import Layout from '../components/layout/layout'
import ThemeContext from '../constants/theme-context';
import PageNotFound from '../components/layout/page-not-found'
import styles from '../styles/story.module.scss'
import api from '../constants/api'
import { getPageData, getPageDetail } from '../library/pages'

export default function About ({ data, pageData }) {
  const { theme } = useContext(ThemeContext)
  if (!data || data.message) {
    return <PageNotFound />
  }

  return (
    <Layout content={data} pageData={pageData}>
      <div className={[styles.container, styles['single']].join(" ")}>
        <div className={styles.block_title}>
          <div className={styles.block_atitle}>
            {data.alt_title && <span className={styles.atitle}>{data.alt_title}</span>}
          </div>
          <h1 className={styles.title}><span className={[styles.highlight, styles[`${theme}`]].join(" ")}>{data.title}</span></h1>
        </div>

        <div className={styles.body_wrapper}>
          <main className={[styles.main, styles['single'], styles[`${theme}`]].join(" ")}>
            <div className={styles.content}>
              <div dangerouslySetInnerHTML={{ __html: data.rendered_body }} />
            </div>
          </main>
        </div>
      </div>
    </Layout>
  )
}

export async function getServerSideProps() {
  const data = await getPageDetail(api.pages.sponsorship)
  const pageData = await getPageData()

  return {
    props: { data, pageData },
  }
}
