const nextConfig = {
    /* config options here */
    rewrites: async () => [
        {
          source: '/server-sitemap.xml',
          destination: '/server-sitemap',
        },
        {
          source: '/server-sitemap-:page.xml',
          destination: '/server-sitemap/:page',
        },
      ],
  }
   
  module.exports = nextConfig