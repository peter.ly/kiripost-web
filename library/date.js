import moment from 'moment'

function initDateObject(value = null) {
  return moment(value ? new Date(value) : new Date())
}

function shorterDateFormat(value = null) {
  return initDateObject(value).format('MMMM D, YYYY')
}

function shortDateFormat(value = null) {
  return initDateObject(value).format('dddd, MMMM D, YYYY')
}

function longDateFormat (value = null) {
  const date = initDateObject(value)
  return `${date.format('MMMM D, YYYY')}`
}

export {
  shorterDateFormat,
  shortDateFormat,
  longDateFormat
}