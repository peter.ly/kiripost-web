import _ from 'lodash'
import { getCollections } from './collections'
import { fetchAPI } from './request'
import { getTopics } from './topics'

async function getPageDetail(url) {
  return await fetchAPI(url)
}

async function getPageData() {
  return {
    collections: await getCollections(),
    topics: await getTopics()
  }
}

function getContentData(content, primary_field, secondary_field) {
  const value = _.get(content, primary_field, '')
  return _.isEmpty(value) ? _.get(content, secondary_field, '') : value
}

export {
  getContentData,
  getPageData,
  getPageDetail
}