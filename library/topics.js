import api from '../constants/api'
import _ from 'lodash'
import { fetchAPI } from './request'
import { applyPagination } from './pagination'

async function getTopics(paginate = false, page = 0) {
  const url = applyPagination(api.topic.list, paginate, page)
  return await fetchAPI(url)
}

async function getTopicDetail(slug) {
  return await fetchAPI(slug + api.topic.detail)
}

async function getTopicStories(id, paginate = false, page = 0) {
  const url = applyPagination(`${api.story.list}&topics=${id}`, paginate, page)
  return await fetchAPI(url)
}

async function getTopicIndex() {
  return await fetchAPI(api.pages.topics)
}

export {
  getTopics,
  getTopicDetail,
  getTopicStories,
  getTopicIndex
}