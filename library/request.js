import _ from 'lodash'
import {
  asset_url,
  api_base_url,
  base_url,
} from '../constants/setting'

async function fetchAPI(api_url) {
  const res = await fetch(api_base_url + api_url);
  const data = await res.json();
  return data
}

function url(path, type = '') {
  return base_url + (type ? '/' + type + '/' : '') + path
}

function asset(path) {
  // todo remove replace
  return _.replace(asset_url + path, 'http://localhost:8000', '')
}

export {
  asset,
  fetchAPI,
  url
}