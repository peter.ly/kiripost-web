import api from '../constants/api'
import _ from 'lodash'
import { fetchAPI } from './request'
import { applyPagination } from './pagination'

async function getCollections(paginate = false, page = 0) {
  const url = applyPagination(api.collection.list, paginate, page)
  return await fetchAPI(url)
}

async function getCollectionDetail(slug) {
  const url = slug + api.collection.detail
  return await fetchAPI(url)
}

async function getCollectionStories(id, paginate = false, page = 0) {
  const url = applyPagination(`${api.story.list}&collections=${id}`, paginate, page)
  return await fetchAPI(url)
}

async function getCollectionIndex() {
  return await fetchAPI(api.pages.collections)
}

export {
  getCollections,
  getCollectionDetail,
  getCollectionStories,
  getCollectionIndex
}