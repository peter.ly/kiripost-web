import _ from 'lodash'
import {
  pagination
} from '../constants/setting'

function applyPagination(api_url, paginate = true, page = 0) {
  if (paginate == false) {
    return api_url
  }

  let offset = _.parseInt(page)
  if (offset > 0) {
    offset -= 1
  }
  offset = offset * pagination.limit

  return `${api_url}&offset=${offset}&limit=${pagination.limit}`
}

function getPaginatedPaths(data, otherParams = {}) {
  const paths = []
  let total = data.meta.total_count

  while (total > 0) {
    paths.push({ params: {...otherParams, page: `${total}`} })
    total -= 1
  }

  return paths
}

function getPages(current = 1, total) {
  const pages = []
  const distance = 2
  let start = current - distance

  // current page
  if (current == 1) {
    start = 2
  } else if (current == total) {
    start = total - distance
  } else {
    start = current - 1
  }
  const end = start + distance

  for(let i = start; i <= end; i++) {
    if (i < total && i > 1) {
      pages.push(i)
    }
  }
  if (pages.length == 3 && _.last(pages) == total) {
    pages.shift()
  }

  // first page
  if (_.first(pages) - 1 >= distance) {
    pages.unshift('...')
  }
  pages.unshift(1)
  if (pages.length > 3 && start == distance) {
    pages.pop()
  }

  // last page
  if (total - _.last(pages) > 1) {
    pages.push('...')
  }
  pages.push(total)


  return pages
}

export {
  applyPagination,
  getPaginatedPaths,
  getPages
}