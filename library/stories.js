import api from '../constants/api'
import { fetchAPI } from './request'
import { applyPagination } from './pagination'

async function getStoriesForHome() {
  return {
    featured: await fetchAPI(api.story.list + '&is_featured=1' + '&limit=5'),
    latest: await fetchAPI(api.story.list + '&limit=4'),
  }
}

async function getStories(paginate = false, page = 0) {
  const url = applyPagination(api.story.list, paginate, page)
  return await fetchAPI(url)
}

async function getLatestStories (paginate = false, page = 0, limit=6, offset = 0) {
  const url = api.story.list + `&limit=${limit}` + `&offset=${offset}`;
  return await fetchAPI(url)
}

async function getStoryByTopics (topicIds, paginate = false, page = 0) {
  let qs = api.story.list + '&limit=6';
  topicIds.forEach(id => {
    qs += `&topics=${id}`;
  });
  return await fetchAPI(qs)
}

async function getFeaturedStories(paginate = false, page = 0, limit=5) {
  const url = `${api.story.list}&is_featured=1&limit=${limit}`
  const paginated_url = applyPagination(url, paginate, page)
  return await fetchAPI(paginated_url)
}

async function getSponsoredStories(paginate = false, page = 0) {
  const url = applyPagination(api.story.list + '&is_sponsored=1', paginate, page)
  return await fetchAPI(url)
}

async function getStoryDetail(slug) {
  return await fetchAPI(slug + api.story.detail)
}

async function getStoryIndex() {
  return await fetchAPI(api.pages.stories)
}

export {
  getStoriesForHome,
  getFeaturedStories,
  getSponsoredStories,
  getStories,
  getStoryDetail,
  getStoryByTopics,
  getStoryIndex,
  getLatestStories
}